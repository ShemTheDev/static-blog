/**
 * Welcome to your Workbox-powered service worker!
 *
 * You'll need to register this file in your web app and you should
 * disable HTTP caching for this file too.
 * See https://goo.gl/nhQhGp
 *
 * The rest of the code is auto-generated. Please don't update this file
 * directly; instead, make changes to your Workbox build configuration
 * and re-run your build process.
 * See https://goo.gl/2aRDsh
 */

importScripts("workbox-v4.3.1/workbox-sw.js");
workbox.setConfig({modulePathPrefix: "workbox-v4.3.1"});

workbox.core.setCacheNameDetails({prefix: "eleventy-plugin-pwa"});

workbox.core.skipWaiting();

workbox.core.clientsClaim();

/**
 * The workboxSW.precacheAndRoute() method efficiently caches and responds to
 * requests for URLs in the manifest.
 * See https://goo.gl/S9QRab
 */
self.__precacheManifest = [
  {
    "url": "404.html",
    "revision": "eeef8cdc718985dfc6baad578cbf2f77"
  },
  {
    "url": "admin/index.html",
    "revision": "c343ce8b02463120013498584325fa1a"
  },
  {
    "url": "img/192.png",
    "revision": "5165034a188fa32440b2844c61f7a7d3"
  },
  {
    "url": "img/512.png",
    "revision": "341bc51357d41f3f4e7bb3f301ca94cb"
  },
  {
    "url": "img/90s.png",
    "revision": "ecab39f69f1ac5521ea8690a4abe78b5"
  },
  {
    "url": "img/android-chrome-192x192.png",
    "revision": "513df11f3daed866d316dd807303a733"
  },
  {
    "url": "img/android-chrome-512x512.png",
    "revision": "a94dfba002ab8679fff96a63c5508f91"
  },
  {
    "url": "img/apple-touch-icon.png",
    "revision": "f2da0bb4bd340e0022a0564e9cc8c0b6"
  },
  {
    "url": "img/favicon-16x16.png",
    "revision": "b9946bd0bffdd6220b24c74f64af2bb8"
  },
  {
    "url": "img/favicon-32x32.png",
    "revision": "8358415aad07d9fcc9169d0898d81be5"
  },
  {
    "url": "img/favicon.ico",
    "revision": "e90434a53a5cf90c5d34c934347980be"
  },
  {
    "url": "index.html",
    "revision": "aacec31565e828a4170255852743e8ce"
  },
  {
    "url": "manifest.json",
    "revision": "c987a64ba6bc06f112736fc222f97b26"
  },
  {
    "url": "posts/elevasion/index.html",
    "revision": "6de0cf95af02132024e9214915efd265"
  },
  {
    "url": "posts/future-of-web-text-centric/index.html",
    "revision": "0f522aaf445609c832abe6842dbe9b05"
  },
  {
    "url": "posts/index.html",
    "revision": "a1e13999e076524f8ac4969670e3024a"
  },
  {
    "url": "posts/just-breathe/index.html",
    "revision": "01be4706343a2713a189da408997286e"
  },
  {
    "url": "posts/mvp-to-v1/index.html",
    "revision": "a7e27cb0367a0d9fabd52516d16fb640"
  },
  {
    "url": "posts/quick-tips-kai-os/index.html",
    "revision": "ae4dc29be31ed4ef2e55f6f16c5b8205"
  },
  {
    "url": "posts/taking-salary-bitcoin/index.html",
    "revision": "949dc09c3aecef3f236a4fa3ddd51667"
  },
  {
    "url": "posts/three-inspirational-websites/index.html",
    "revision": "234fc6746cac059f78cdf5aad4726558"
  },
  {
    "url": "projects/Airtable-Portfolio/index.html",
    "revision": "bc8280376452e776d4d8c271f5463422"
  },
  {
    "url": "projects/Future-Rally/index.html",
    "revision": "0caaaa288dad3f9fa16de2057dedcac4"
  },
  {
    "url": "projects/index.html",
    "revision": "cee150328a13100a191d65dd027e9b0a"
  },
  {
    "url": "projects/lucid-dreaming-life-hacks/index.html",
    "revision": "bfb632466af1b440c3d19956643c027c"
  },
  {
    "url": "projects/Noverantale.com/index.html",
    "revision": "50a0f957ecf2bde730fbd98684b3af30"
  },
  {
    "url": "projects/Sagasu/index.html",
    "revision": "d32ec9cd84cc40288241abefd5c13da8"
  },
  {
    "url": "projects/Smashesque/index.html",
    "revision": "f1a9d02d0e4bb3af42a40df50dccce48"
  },
  {
    "url": "tags/Airtable/index.html",
    "revision": "d48ce1c61f58476622dc3fea6d6672ee"
  },
  {
    "url": "tags/bitcoin/index.html",
    "revision": "5cbe29b68871125677131ca1aa9478cf"
  },
  {
    "url": "tags/careers/index.html",
    "revision": "4c1344e10c08397c4a07fbd52cacfba1"
  },
  {
    "url": "tags/crypto/index.html",
    "revision": "ab765cfa7d8fc53b7d0fec2c3de98871"
  },
  {
    "url": "tags/design/index.html",
    "revision": "9541358a4f3636eb43d18e55e89d7adb"
  },
  {
    "url": "tags/game maker/index.html",
    "revision": "2bcd0fd31c410d5c5cea8b98e281b22c"
  },
  {
    "url": "tags/gamedev/index.html",
    "revision": "fa5acc85afda70c864722d441b253236"
  },
  {
    "url": "tags/index.html",
    "revision": "0759f83e40a29c1d9b22d760d0875650"
  },
  {
    "url": "tags/KaiOS/index.html",
    "revision": "fb7a369e5ab8977e0f380424d9ae6253"
  },
  {
    "url": "tags/mindfulness/index.html",
    "revision": "7df2c836603d9ea738f3b79002d2d5a8"
  },
  {
    "url": "tags/projects/index.html",
    "revision": "b9033bdca3a71959547b4807831225b4"
  },
  {
    "url": "tags/react/index.html",
    "revision": "f87c303a02c56a11b95466a491c807f0"
  },
  {
    "url": "tags/scope/index.html",
    "revision": "c14a0f2bcc198509a544979f93181c81"
  },
  {
    "url": "tags/webapp/index.html",
    "revision": "a601bda1a6e18eba41067b5b1004fee3"
  },
  {
    "url": "tags/webdev/index.html",
    "revision": "ac08892296835cd8b5672d427f062587"
  }
].concat(self.__precacheManifest || []);
workbox.precaching.precacheAndRoute(self.__precacheManifest, {});

workbox.routing.registerRoute(/^.*\.(html|jpg|png|gif|webp|ico|svg|woff2|woff|eot|ttf|otf|ttc|json)$/, new workbox.strategies.StaleWhileRevalidate(), 'GET');
workbox.routing.registerRoute(/^https?:\/\/fonts\.googleapis\.com\/css/, new workbox.strategies.StaleWhileRevalidate(), 'GET');
